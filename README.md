# Custom Executor Testing project

Overview of This Repo: 

* chroot_shell and setuid_shell directories contain sample scripts to be installed on the runner machine 
* config.toml.example files show what a working configuration looks like 
* this README.md file walks through the process 
* The .gitlab-ci.yml file runs several different jobs that should succeed and fail in different ways to demonstrate the runner working

The recommended approach to configuring the runner is to fork this repository and configure it to run jobs on the new runner.  Once that works, you can remove the copy of this repo and start using your own gitlab-ci pipeline definitions on your projects. 

# important note: the `$GITLAB_USER_LOGIN` and other environment variables can be overwritten by pipeline definition, project variables, or even pipeline-launch screen. 

MR for fix: https://gitlab.com/gitlab-org/gitlab-runner/issues/4677



# important note: the `$GITLAB_USER_LOGIN` and other environment variables can be overwritten by pipeline definition, project variables, or even pipeline-launch screen. 

MR for fix: https://gitlab.com/gitlab-org/gitlab-runner/issues/4677


## Pipeline

There will be two stages in the pipeline, the first should pass all the tests.
The second should fail all the tests.

### Stage 1: should_pass

Each test here validates a different happy path through the system. These can
also be used to show stuff like "whoami" and environment variables.

### Stage 2: should_fail

Each test here fails in a different way. The name of the job should follow the
convention to show whether the failure is from the custom exec wrapper or the job.
It may be difficult to tell the difference, though error code defined by the
`$CUSTOM_ENV_BUILD_FAILURE_EXIT_CODE` environment variable should be for a job
that fails because the Runner job died and an error code defined by the
`$CUSTOM_ENV_SYSTEM_FAILURE_EXIT_CODE` environment variable should show
any time the wrapper scripts fails.

## Configuring Custom Executor

### Prerequisites

1. Install GitLab runner on a linux box running a recent [Linux distribution](https://docs.gitlab.com/runner/#install-gitlab-runner). 
2. Fork this project into your own namespace and create a Project settings > CI variable for `$PROJECT_USER`.
3. Create project users and add them to a group called `runner`.  Steps to do this on Debian/Ubuntu are at the end of the readme. 
4. Follow the instructions below to configure the custom executor

The below instructions assume there is a gitlab-runner user who has a home directory in `/home/gitlab-runner` which is how the Debian package installs the runner package. 

### Use a fork of this repo the first time

To be sure everything is working right, use this repo or change the URL below to your own fork. 

```bash
cd /home/gitlab-runner
git clone git@gitlab.com:MYUSERNAME/custom-executor-test.git ce
```

### Register the custom executor

The registration process will insert values into the configuration file for most of the custom executor fields.  You will need to add `builds_dir`, `cache_dir`, and an optional `config_inject` value. 

For SetUID with `su` example: 

```bash
sudo gitlab-runner register \
    --url https://gitlab.com \
    --registration-token __REDACTED__ \
    --description custom-setuid \
    --tag-list custom,setuid,shell \
    --run-untagged \
    --executor custom \
    --custom-prepare-exec /home/gitlab-runner/ce/setuid_shell/prepare.sh \
    --custom-run-exec /home/gitlab-runner/ce/setuid_shell/run.sh \
    --custom-cleanup-exec /home/gitlab-runner/ce/setuid_shell/cleanup.sh
```

For chroot example: 

```bash
sudo gitlab-runner register \
    --url https://gitlab.com \
    --registration-token __REDACTED__ \
    --description custom-chroot \
    --tag-list custom,setuid,chroot \
    --executor custom \
    --custom-prepare-exec /home/gitlab-runner/ce/chroot_shell/prepare.sh \
    --custom-run-exec /home/gitlab-runner/ce/chroot_shell/run.sh \
    --custom-cleanup-exec /home/gitlab-runner/ce/chroot_shell/cleanup.sh
```

You'll see the script paths, tags, and `--run-untagged` vary between the registrations. 
These can both be run on the same system.  Generically I'd avoid having untagged runners 
hit these jobs unless it's added to a specific project.

**NOTE** Remember to set `builds_dir` and `cache_dir` in the `/etc/gitlab-runner/config.toml` file. See the examples in the related executor directory.

Manually registering would look like this: 

```bash
$ sudo gitlab-runner register
Runtime platform                                    arch=amd64 os=linux pid=3864 revision=fa5aad7d version=12.1.0~beta.1678.gfa5aad7d
Running in system-mode.

Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com/):
https://gitlab.com/
Please enter the gitlab-ci token for this runner:
[__REDACTED__]:
Please enter the gitlab-ci description for this runner:
[custom-setuid]:
Please enter the gitlab-ci tags for this runner (comma separated):
[custom,setuid,shell]:
Registering runner... succeeded                     runner=sykTz6Ta
Please enter the executor: custom, docker, shell, virtualbox, docker+machine, docker-ssh+machine, kubernetes, docker-ssh, parallels, ssh:
[custom]:
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
```

After the manual registration, you must add the script paths to `/etc/gitlab-runner/config.toml`. 

## Test the custom executor scripts

Rather than issuing the `start` command, use `run` so you can watch it retrieve and
process jobs. This will help with problems such as execute access or file permissions.

When you're done watching the execution, hitting `CTRL-C` to end the process will take a little while to stop the service gracefully.  If it takes too long, you can hit `CTRL-C` a bunch more times to kill it quicker. 

# Schedule a job

Make a commit in your fork and it'll kick off...  best of luck!

# Extra Configurations for testing

## Setting up some exec users

```bash
sudo addgroup runner

sudo useradd -m -d /home/execuser -s /bin/bash -U execuser
sudo useradd -m -d /home/execuser2 -s /bin/bash -U execuser2

sudo adduser execuser runner
sudo adduser execuser2 runner
```

So now both users are in the group where they can pass the run stage evaluation. 

Forcing the users to be in the runner group prevents someone from using "root" as their `PROJECT_USER`.