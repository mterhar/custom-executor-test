#!/bin/bash
########################
#      Validate user
########################

[[ -z "${CUSTOM_ENV_PROJECT_USER_OVERRIDE}" ]] && EXECUTION_USER=$CUSTOM_ENV_PROJECT_USER || EXECUTION_USER=$CUSTOM_ENV_PROJECT_USER_OVERRIDE

id -u $EXECUTION_USER 1> /dev/null
if [[ ${?} -ne 0 ]]; then
  echo "no PROJECT_USER or _OVERRIDE found"
  id -u $CUSTOM_ENV_GITLAB_USER_LOGIN 1> /dev/null
  if [ $? -ne 0 ]; then
    echo "User does not exist"
    exit 1
  else
    EXECUTION_USER=$CUSTOM_ENV_GITLAB_USER_LOGIN
    echo "Using committer user = ${CUSTOM_ENV_GITLAB_USER_LOGIN}"
  fi
fi

# Check if user is in a `runner` group
if groups $EXECUTION_USER | grep &> /dev/null '\brunner\b'; then
  echo "${EXECUTION_USER} is in runner group, execution is allowed"
else
  echo "${EXECUTION_USER} is not allowed to be a runner user"
  exit 1
fi

########################
#      Setup Base dir
########################

BASE_DIR=/home/gitlab-runner/custom-executor/builds/$EXECUTION_USER/$CUSTOM_ENV_CI_PROJECT_PATH_SLUG/$CUSTOM_ENV_CI_BUILD_REF_NAME/

echo 'Prepare builds_dir and cache_dir'
mkdir -p ${BASE_DIR}${BUILDS_DIR}
mkdir -p ${BASE_DIR}${CACHE_DIR}

chown -R ${EXECUTION_USER} ${BASE_DIR}
chown -R $EXECUTION_USER /home/gitlab-runner/custom-executor/builds/$EXECUTION_USER
chown -R $EXECUTION_USER $TMPDIR
echo 'Updated build and temp dir permissions'

cd $BASE_DIR

# Execute the script generate by GitLab.
su -c "/bin/bash ${1}" $EXECUTION_USER
