#!/usr/bin/env bash

set -eo pipefail

trap "exit ${CUSTOM_ENV_SYSTEM_FAILURE_EXIT_CODE}" ERR

########################
#      Validate user
########################

[[ -z "${CUSTOM_ENV_PROJECT_USER_OVERRIDE}" ]] && EXECUTION_USER=$CUSTOM_ENV_PROJECT_USER || EXECUTION_USER=$CUSTOM_ENV_PROJECT_USER_OVERRIDE

id -u $EXECUTION_USER 1> /dev/null
if [[ ${?} -ne 0 ]]; then
  id -u $CUSTOM_ENV_GITLAB_USER_LOGIN 1> /dev/null
  if [ $? -ne 0 ]; then
    exit 1
  else
    EXECUTION_USER=$CUSTOM_ENV_GITLAB_USER_LOGIN
  fi
fi

cat << EOS
{
  "builds_dir": "/home/${EXECUTION_USER}/builds/${CUSTOM_ENV_CI_CONCURRENT_PROJECT_ID}/${CUSTOM_ENV_CI_PROJECT_PATH_SLUG}",
  "cache_dir": "/home/${EXECUTION_USER}/cache/${CUSTOM_ENV_CI_CONCURRENT_PROJECT_ID}/${CUSTOM_ENV_CI_PROJECT_PATH_SLUG}",
  "builds_dir_is_shared": false
}
EOS
