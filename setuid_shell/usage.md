Security
==========

The config.toml, clean.sh, prepare.sh, and run.sh scripts all run as the GitLab-
runner user.  They should be read-only for the gitlab-runner user. 

At some point in the run.sh file (suggest near the end), a call to trigger the script should be included:

```
su -c "/bin/bash ${1}" $EXECUTION_USER
```

* `su -c` is the regular linux command to run a command as another user
* `${1}` is the path to the script in the /tmp directory which is a combination of the gitlab-ci.yml specified "before_script" + the current job's script. 
* `$EXECUTION_USER` sets the user account that has already been vetted by the example run script

Keeping Outputs
==========

When saving things to the user's local folder: 

* `~` works to get to the su-context user's home directory 
* `/home/gitlab-runner/custom-executor/builds/[user]/[project]/[branch]/` the configuration script creates a build directory which can be left alone for the user to retrieve information from
* If moving data to `~`, use a specific directory for the job to avoid collisions: `$CI_JOB_ID`
* Setting `artifacts` in the gitlab-ci file will provide them to future jobs and upload the artifacts to the server
* Setting `cache` in the gitlab-ci file will store files between execution phases
* Giving the `cache` a key based on the branch or project will persist the cache for longer

Environment Variable Prefixes
==========

The Generic Executor was renamed to "Custom Executor" so that may lead to the prefix changing to `CUSTOM_ENV`. 