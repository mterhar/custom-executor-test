#!/usr/bin/env bash

set -eo pipefail

trap "exit ${SYSTEM_FAILURE_EXIT_CODE}" ERR

CUR_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source ${CUR_DIR}/base.sh

umount ${BASE_DIR}/dev/pts
umount ${BASE_DIR}/dev
umount ${BASE_DIR}/proc

echo "what is left after unmount:"

cd ${BASE_DIR}

echo "pwd:"
pwd

echo "ls -la *:"
ls -al *

echo "rm -rf ./*:"
rm -rf ./*

