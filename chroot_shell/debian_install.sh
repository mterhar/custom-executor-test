#!/usr/bin/env bash

set -eo pipefail

CUR_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

SKEL_DIR=${SKEL_DIR:-${CUR_DIR}/chroot_skel}
ARCH=${ARCH:-amd64}
RELEASE=${RELEASE:-stretch}
MIRROR=${MIRROR:-http://deb.debian.org/debian}

echo "Updating Debian"
apt-get update
apt-get upgrade -y

echo "Installing debootstrap"
apt-get install -y binutils debootstrap

echo "Creating SKEL_DIR=${SKEL_DIR} directory"
if [[ -d ${SKEL_DIR} ]]; then
    echo "SKEL_DIR=${SKEL_DIR} already exists!"
    exit 1
fi

mkdir -p ${SKEL_DIR}

echo "Debootstrap-ing ${SKEL_DIR}"
debootstrap --arch ${ARCH} ${RELEASE} ${SKEL_DIR} ${MIRROR}

echo "Installing required packages inside of the SKEL_DIR"
chroot ${SKEL_DIR} /bin/bash <<EOF
apt-get install -y git wget
wget https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64 -O /usr/local/bin/gitlab-runner
chmod +x /usr/local/bin/gitlab-runner
EOF

