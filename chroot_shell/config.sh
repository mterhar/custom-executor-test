#!/usr/bin/env bash

set -eo pipefail

trap "exit ${CUSTOM_ENV_SYSTEM_FAILURE_EXIT_CODE}" ERR

CUR_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source ${CUR_DIR}/base.sh

echo "{\"builds_dir\":\"${BUILDS_DIR}\",\"cache_dir\":\"${CACHE_DIR}\"}"


