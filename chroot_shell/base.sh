#!/usr/bin/env bash

########################
#      Validate user
########################

[[ -z "${CUSTOM_ENV_PROJECT_USER_OVERRIDE}" ]] && EXECUTION_USER=$CUSTOM_ENV_PROJECT_USER || EXECUTION_USER=$CUSTOM_ENV_PROJECT_USER_OVERRIDE

id -u $EXECUTION_USER 1> /dev/null
if [[ ${?} -ne 0 ]]; then
  id -u $CUSTOM_ENV_GITLAB_USER_LOGIN 1> /dev/null
  if [ $? -ne 0 ]; then
    exit 1
  else
    EXECUTION_USER=$CUSTOM_ENV_GITLAB_USER_LOGIN
  fi
fi

# echo "base.sh found execution user = ${EXECUTION_USER}"
BASE_DIR=/home/${EXECUTION_USER}/ce-chroot/${CUSTOM_ENV_CI_CONCURRENT_PROJECT_ID}/${CUSTOM_ENV_CI_PROJECT_PATH_SLUG}/${CUSTOM_ENV_CI_COMMIT_REF_SLUG}

# This will be used inside of the chroot
BUILDS_DIR=/home/${EXECUTION_USER}/builds
CACHE_DIR=/home/${EXECUTION_USER}/cache
