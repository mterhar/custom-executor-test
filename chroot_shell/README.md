## Preparation on Debian

First, prepare the chroot setup. The automaisation is prepared for Debian based
systems, where `debootstrap` prepares the skeleton of the system.

The output of the `debian_install.sh` command below is a directory that contains 
enough to be able to execute jobs and call the runner command to process cache 
and artifacts. 

First: cd into this directory. 

```bash
./debian_install.sh
# [ ... does a lot ... ]
$ du -hs chroot_skel/
453M	chroot_skel/
```

For this run `./debian_install.sh` inside of the `chroot_shell` directory of this
project's local working copy.

Next, prepare a `config.toml` file using the `config.toml.example` as a reference.
For basic setup the only thing that should be adjusted are the paths inside of the
`[runners.custom]` section.

## Test the Runner

The `gitlab-runner start` command starts the service to run jobs, but lacks visibility. 

If it's running already, be sure to stop it: `gitlab-runner stop`

To run a test configuration and monitor the job progress, start the Runner with 
`gitlab-runner run --config /path/to/config.toml`. 

## Update the service configuration

Put the contents of your custom `config.toml` file into `/etc/gitlab-runner/config.toml`.

Run `gitlab-runner restart`. 

## Make a `chroot_skel` without Debian

Details need to be fleshed out. Important aspects for GitLab: 

1. The inside of the chroot needs the same `gitlab-runner` executable in the path variable
2. Other binaries from `/bin`, `/sbin`, and `/usr/[s]bin` are probably needed for normal functioning 
3. Put the "git" binary in there 
4. Anything needed to run compilation should be included 

## Known problems

The CHROOT approach has some problems:

1. Mounting with Bind is difficult to unmount, ends up with lots of extra mount points that won't `umount`.

