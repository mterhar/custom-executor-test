#!/usr/bin/env bash

set -eo pipefail

trap "exit ${SYSTEM_FAILURE_EXIT_CODE}" ERR

CUR_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source ${CUR_DIR}/base.sh

# Check if user is in a `runner` group
if groups $EXECUTION_USER | grep &> /dev/null '\brunner\b'; then
  echo "${EXECUTION_USER} is in runner group, execution is allowed"
else
  echo "${EXECUTION_USER} is not allowed to be a runner user"
  exit 1
fi

mkdir -p ${BASE_DIR}
echo "preparation starting"

SKEL_DIR=${SKEL_DIR:-${CUR_DIR}/chroot_skel}

echo "SKEL_DIR=${SKEL_DIR}"

cp -a ${SKEL_DIR}/* ${BASE_DIR}/

chown $EXECUTION_USER ${BASE_DIR}

cp /etc/hosts ${BASE_DIR}/etc/hosts
cp /etc/resolv.conf ${BASE_DIR}/etc/resolv.conf

mount --bind /dev ${BASE_DIR}/dev
mount --bind /dev/pts ${BASE_DIR}/dev/pts
mount --bind /proc ${BASE_DIR}/proc

echo "preparation complete"

