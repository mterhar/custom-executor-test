#!/usr/bin/env bash

set -eo pipefail

trap "exit ${SYSTEM_FAILURE_EXIT_CODE}" ERR

CUR_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source ${CUR_DIR}/base.sh

# Check if user is in a `runner` group
if groups $EXECUTION_USER | grep &> /dev/null '\brunner\b'; then
  echo "${EXECUTION_USER} is in runner group, execution is allowed"
else
  echo "${EXECUTION_USER} is not allowed to be a runner user"
  exit 1
fi

echo "Handling ${2} stage"

TEMP_SCRIPT_VAR=${BASE_DIR}/gitlab-ci.sh
echo ${1}
cp ${1} ${TEMP_SCRIPT_VAR}
chmod a+x ${TEMP_SCRIPT_VAR}

echo 'Prepare builds_dir and cache_dir'
mkdir -p ${BASE_DIR}${BUILDS_DIR}
mkdir -p ${BASE_DIR}${CACHE_DIR}

echo 'Updating build and temp dir permissions'
chown ${EXECUTION_USER} ${BASE_DIR}
chown ${EXECUTION_USER} ${TEMP_SCRIPT_VAR}
chown ${EXECUTION_USER} ${BASE_DIR}${BUILDS_DIR}
chown ${EXECUTION_USER} ${BASE_DIR}${CACHE_DIR}
chown -R ${EXECUTION_USER} ${TMPDIR}

# Arbitrary commands inside chroot.
chroot --userspec ${EXECUTION_USER}:${EXECUTION_USER} ${BASE_DIR} /bin/bash <<EOF
echo "inside preparation chroot >>"
pwd
id
echo "<< end of preparation chroot"
EOF

echo "execute script in the same chroot >>"

# Execute the script generate by GitLab.
errorCode=0

chroot --userspec ${EXECUTION_USER}:${EXECUTION_USER} ${BASE_DIR} /bin/bash /gitlab-ci.sh || errorCode=1
echo "<< end of script execution in chroot"

if [[ ${errorCode} -ne 0 ]]; then
    exit ${BUILD_FAILURE_EXIT_CODE}
fi

